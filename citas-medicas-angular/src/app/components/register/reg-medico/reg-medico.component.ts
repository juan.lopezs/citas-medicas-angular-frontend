import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Medico } from 'src/app/model/medico';
import { Medico2 } from 'src/app/model/medico2';
import { RegisterService } from 'src/app/service/register.service';

@Component({
  selector: 'app-reg-medico',
  templateUrl: './reg-medico.component.html',
  styleUrls: ['./reg-medico.component.css']
})
export class RegMedicoComponent implements OnInit {

  registerForm : FormGroup;

  constructor(
    private registerService : RegisterService,
    private router : Router,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit(): void { 
    this.registerForm = this.formBuilder.group({
      usuario : ['', [Validators.required]],
      clave : ['', [Validators.required]],
      clave2 : ['', [Validators.required]],
      nombre : ['', [Validators.required]],
      apellidos : ['', [Validators.required]],
      numColegiado : ['', [Validators.required]]
    })
  }

  onSubmit(){
    //console.log('Register medico');
    //console.log(this.registerForm.value);

    // Comprobamos que todos los campos != ""
    for (let key in this.registerForm.value) {
      if(this.registerForm.value[key] == ""){
        alert('Todos los campos deben estar rellenos');
        return;
      }
    }
    
    // clave == clave2
    if(this.registerForm.value['clave'] != this.registerForm.value['clave2']){
      alert('Las claves deben coincidir');
      return;
    }

    // Registramos el formulario
    this.registerService.registrarMedico(this.registerForm.value).subscribe(medico => {
      //console.log("Medico registrado:");
      //console.log(medico);
      if(!medico.usuario){
        alert("Error. El usuario ya está registrado.");
         
      } else {
        alert("Registrado con éxito.");
        this.router.navigate(['login']);  
      }
    })

  }

  get getControl(){
    return this.registerForm.controls;
  }
}
