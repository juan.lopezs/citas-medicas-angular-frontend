import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/service/register.service';

@Component({
  selector: 'app-reg-paciente',
  templateUrl: './reg-paciente.component.html',
  styleUrls: ['./reg-paciente.component.css']
})
export class RegPacienteComponent implements OnInit {

  registerForm : FormGroup;

  constructor(
    private registerService : RegisterService,
    private router : Router,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit(): void { 
    this.registerForm = this.formBuilder.group({
      usuario : ['', [Validators.required]],
      clave : ['', [Validators.required]],
      clave2 : ['', [Validators.required]],
      nombre : ['', [Validators.required]],
      apellidos : ['', [Validators.required]],
      nss : ['', [Validators.required]],
      numTarjeta : ['', [Validators.required]],
      telefono : ['', [Validators.required]],
      direccion : ['', [Validators.required]]
    })
  }

  onSubmit(){
    //console.log('Register paciente');
    //console.log(this.registerForm.value);

    // Comprobamos que todos los campos != ""
    for (let key in this.registerForm.value) {
      if(this.registerForm.value[key] == ""){
        alert('Todos los campos deben estar rellenos');
        return;
      }
    }

    // clave == clave2
    if(this.registerForm.value['clave'] != this.registerForm.value['clave2']){
      alert('Las claves deben coincidir');
      return;
    }

    delete this.registerForm.value['clave2']; // No hay que pasar la clave repetida
    // Registramos el formulario
    this.registerService.registrarPaciente(this.registerForm.value).subscribe(paciente => {
      
      if(!paciente.usuario){
        alert("El usuario ya está registrado");
        return;
      }
      
      alert("Paciente registrado con exito");
      this.router.navigate(['login']);
    }); 
    /*
    if(!user || !clave || !nombre || !apellidos || !nss || !numTarjeta || !telefono || !direccion){
      alert('Todos los campos deben estar completos');
      return;
    }

    var p = new Paciente(user, clave, nombre, apellidos, nss, numTarjeta, telefono, direccion, [], []);

    this.registerService.registrarPaciente(p).subscribe(paciente => {
      
      if(!paciente.usuario){
        alert("El usuario ya está registrado");
        return;
      }
      
      alert("Paciente registrado con exito");
      this.router.navigate(['login']);
    });*/
  }



  get getControl(){
    return this.registerForm.controls;
  }
}
