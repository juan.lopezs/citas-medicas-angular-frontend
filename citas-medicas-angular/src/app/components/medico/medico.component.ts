import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cita } from 'src/app/model/cita';
import { Medico } from 'src/app/model/medico';
import { DiagnosticoService } from 'src/app/service/diagnostico.service';
import { UserActService } from 'src/app/service/user-act.service';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

  medico : Medico;

  constructor(
    private userService : UserActService,
    private diagnosticoService : DiagnosticoService,
    private router : Router
  ) { }

  ngOnInit(): void {
    // Comprobamos el acceso al medico logeado
    this.userService.getMedico().subscribe(medico => {
      if(medico == undefined) {
        this.router.navigate(['login']);
        return;
      }
        
      this.medico = medico;
    });
  }

  updateDiagnostico(d : string, c : Cita){
    // Obtenemos el diagnostico de la BD
    this.diagnosticoService.getDiagnostico(d).subscribe(diagnostico => {
      if(diagnostico === undefined){
        console.log('Error en diagnostico');
        return;
      }
      // Guardamos el diagnostico y la cita
      this.userService.setDiagnostico(diagnostico);
      this.userService.setCita(c);
      this.router.navigate(['medico-diag'])
    })
    
  }

  // Logout
  logout(){
    this.userService.setUndefined();
    this.router.navigate(['login']);
  }
}
