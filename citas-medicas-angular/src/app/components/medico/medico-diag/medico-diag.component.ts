import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Cita } from 'src/app/model/cita';
import { Diagnostico } from 'src/app/model/diagnostico';
import { DiagnosticoService } from 'src/app/service/diagnostico.service';
import { UserActService } from 'src/app/service/user-act.service';

@Component({
  selector: 'app-medico-diag',
  templateUrl: './medico-diag.component.html',
  styleUrls: ['./medico-diag.component.css']
})
export class MedicoDiagComponent implements OnInit {

  diagnostico : Diagnostico;
  cita : Cita;
  diagForm : FormGroup;

  constructor(
    private userService : UserActService,
    private diagService : DiagnosticoService,
    private router : Router,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit(): void {
    // Recuperamos el diagnostico y la cita
    this.userService.getDiagnostico().subscribe(d => {
      if(d == undefined){
        this.router.navigate(['medico'])
      }
      this.diagnostico = d;
    })

    this.userService.getCita().subscribe(c => {
      if(c == undefined){
        this.router.navigate(['medico']);
      }
      this.cita = c;
    })



    // Inicializamos el formulario
    this.diagForm = this.formBuilder.group({
      id : [this.diagnostico.id, [Validators.required]],

      valoracionEspecialista : [this.diagnostico.valoracionEspecialista, [Validators.required]],
      enfermedad : [this.diagnostico.enfermedad, [Validators.required]],
      cita : [this.diagnostico.cita, [Validators.required]]
    })
  }


  saveDiagnostico(){
    // Guardamos el diagnostico
    this.diagService.updateDiagnostico(this.diagForm.value).subscribe(r => {
      alert("Actualizado con éxito");
    });
  }
  
  volver(){
    this.router.navigate(['medico']);
  }

}
