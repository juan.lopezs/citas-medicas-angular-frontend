import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicoDiagComponent } from './medico-diag.component';

describe('MedicoDiagComponent', () => {
  let component: MedicoDiagComponent;
  let fixture: ComponentFixture<MedicoDiagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicoDiagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicoDiagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
