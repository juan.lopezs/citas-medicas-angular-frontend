import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Cita } from 'src/app/model/cita';
import { Medico } from 'src/app/model/medico';
import { Paciente } from 'src/app/model/paciente';
import { CitaService } from 'src/app/service/cita.service';
import { UserActService } from 'src/app/service/user-act.service';

@Component({
  selector: 'app-paciente-cita',
  templateUrl: './paciente-cita.component.html',
  styleUrls: ['./paciente-cita.component.css']
})
export class PacienteCitaComponent implements OnInit {

  paciente : Paciente;
  medico : Medico;
  citaForm : FormGroup;

  constructor(
    private router : Router,
    private userService : UserActService,
    private citaService : CitaService,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit(): void {
    this.userService.getPaciente().subscribe(paciente => {
      if(paciente == undefined) {
        this.router.navigate(['login'])
        return;
      }
        
      this.userService.getCitaMedico().subscribe(m => {
        this.medico = m;
      })
  
      this.userService.getPaciente().subscribe(p => {
        this.paciente = p;
      })
  
      this.citaForm = this.formBuilder.group({
        pacienteId : [this.paciente.usuario, [Validators.required]],
        medicoId : [this.medico.usuario, [Validators.required]],
        fechaHora : ['', [Validators.required]],
        motivoCita : ['', [Validators.required]]
      })
    });
  }

  volver(){
    this.router.navigate(['paciente']);
  }

  onSubmit(){
    
    this.citaService.createCita(this.citaForm.value).subscribe(r => {
      if(r){
        alert("Cita registrada con éxito.");
        this.userService.updatePaciente();
        this.router.navigate(['paciente']);
      }
    });

  }


  get getControl(){
    return this.citaForm.controls;
  }
}
