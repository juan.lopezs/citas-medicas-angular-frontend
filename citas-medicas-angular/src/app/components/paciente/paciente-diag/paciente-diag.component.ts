import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Diagnostico } from 'src/app/model/diagnostico';
import { UserActService } from 'src/app/service/user-act.service';

@Component({
  selector: 'app-paciente-diag',
  templateUrl: './paciente-diag.component.html',
  styleUrls: ['./paciente-diag.component.css']
})
export class PacienteDiagComponent implements OnInit {

  diagnostico : Diagnostico;

  constructor(
    private userService : UserActService,
    private router : Router
  ) { }

  ngOnInit(): void {
    
    this.userService.getPaciente().subscribe(paciente => {
      if(paciente == undefined) {
        this.router.navigate(['login'])
        return;
      }

      this.userService.getDiagnostico().subscribe(d => {
        console.log(d);
        /*if(d == undefined){
          this.router.navigate(['paciente']);
          return;
        }*/
  
        this.diagnostico = d;
      })
      
    });
    
    
    
    
  }

  volver(){
    this.router.navigate(['paciente'])
  }
}
