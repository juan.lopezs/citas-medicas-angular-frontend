import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PacienteDiagComponent } from './paciente-diag.component';

describe('PacienteDiagComponent', () => {
  let component: PacienteDiagComponent;
  let fixture: ComponentFixture<PacienteDiagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PacienteDiagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PacienteDiagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
