import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Diagnostico } from 'src/app/model/diagnostico';
import { Medico } from 'src/app/model/medico';
import { Medico2 } from 'src/app/model/medico2';
import { Paciente } from 'src/app/model/paciente';
import { DiagnosticoService } from 'src/app/service/diagnostico.service';
import { UserActService } from 'src/app/service/user-act.service';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {


  paciente : Paciente;
  
  constructor(
    private userService : UserActService,
    private diagnosticoService : DiagnosticoService,
    private router : Router
  ) { }

  ngOnInit(): void {
     this.userService.getPaciente().subscribe(paciente => {
      if(paciente == undefined) {
        this.router.navigate(['login'])
        return;
      }
        
      this.paciente = paciente;
    });
  }

  addCita(m : Medico2){
    
    console.log('Añadiendo cita con medico = ' + m);
    this.userService.setCitaMedico(m as Medico);
    this.router.navigate(['paciente-cita'])
  }


  viewDiagnostico(id : string){

    this.diagnosticoService.getDiagnostico(id).subscribe(diagnostico => {
      if(diagnostico === undefined){
        console.log('Error en diagnostico');
        return;
      }
      this.userService.setDiagnostico(diagnostico);
      this.router.navigate(['paciente-diag']);
    })
    
    
  }

  logout(){
    this.userService.setUndefined();
    this.router.navigate(['login']);
  }  
}
