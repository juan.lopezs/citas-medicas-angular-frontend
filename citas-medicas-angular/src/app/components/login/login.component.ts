import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/model/usuario';
import { LoginService } from 'src/app/service/login.service';
import { UserActService } from 'src/app/service/user-act.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;

  constructor(
    private loginService : LoginService, 
    private userService : UserActService,
    private router : Router,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit(): void { 
    this.userService.setUndefined();
    // Inicializamos el formulario
    this.loginForm = this.formBuilder.group({
      usuario : ['', [Validators.required]],
      clave : ['', [Validators.required]]
    })
  }

 

  onSubmit(){

    // Mandamos el formulario
    this.loginService.findUsuario(this.loginForm.value).subscribe(usuario =>{
      // Caso de un Medico
      if('numColegiado' in usuario){
        this.userService.setMedico(usuario);
        this.router.navigate(['medico']);
        // Caso de un Paciente
      } else if('nss' in usuario){
        this.userService.setPaciente(usuario);
        this.router.navigate(['paciente']);
        // Error  
      } else {
        alert("Error al iniciar sesion");
      }
    })
  }

  // Para manejar el formulario en el html
  get getControl(){
    return this.loginForm.controls;
  }
}
