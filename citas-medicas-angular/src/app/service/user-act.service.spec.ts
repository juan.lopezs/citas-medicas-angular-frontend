import { TestBed } from '@angular/core/testing';

import { UserActService } from './user-act.service';

describe('UserActService', () => {
  let service: UserActService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserActService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
