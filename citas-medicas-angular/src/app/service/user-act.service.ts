import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Cita } from '../model/cita';
import { Diagnostico } from '../model/diagnostico';
import { Medico } from '../model/medico';
import { Paciente } from '../model/paciente';

@Injectable({
  providedIn: 'root'
})
export class UserActService {

  // Guarda el Usuario logeado (tanto Paciente como Medico)
  private usuarioActual = new BehaviorSubject<any>(undefined);

  // Guarda la Cita que se actualizará (desde paciente-cita)
  private citaActual = new BehaviorSubject<any>(undefined);

  // Guarda el Medico correspondiente a la Cita anterior 
  private citaMedico = new BehaviorSubject<any>(undefined);

  // Guarda el Diagnostico a modificar
  private diagnosticoActual = new BehaviorSubject<any>(undefined);

  constructor(
    private http : HttpClient
  ) { }



  setPaciente(paciente : Paciente){
    // Ordenamos las citas según fechaHora (más próximas primero)
    paciente.citas.sort((a: Cita, b: Cita) => {
      return +new Date(a.fechaHora) - +new Date(b.fechaHora)
    })

    // Ordenamos los medicos según numColegiado
    paciente.medicos.sort((a: Medico, b: Medico) => {
      return parseInt(a.numColegiado) - parseInt(b.numColegiado);
    })

    this.usuarioActual.next(paciente);
  }
  
  setMedico(medico : Medico){
    medico.citas.sort((a: Cita, b: Cita) => {
      return +new Date(a.fechaHora) - +new Date(b.fechaHora)
    })

    medico.pacientes.sort((a: Paciente, b: Paciente) => {
      return parseInt(a.nss) - parseInt(b.nss);
    })


    this.usuarioActual.next(medico);
  }

  getPaciente() : Observable<Paciente> {
    return this.usuarioActual.asObservable();
  }

  getMedico() : Observable<Medico> {
    return this.usuarioActual.asObservable();
  }

  // En logout --> usuario actual = undefined
  setUndefined() : void {
    this.usuarioActual.next(undefined);
  }

  // Cita
  setCita(cita : Cita){
    this.citaActual.next(cita);
  }

  setCitaMedico(medico : Medico){
    this.citaMedico.next(medico);
  }

  getCita() : Observable<Cita> {
    return this.citaActual.asObservable();
  }

  getCitaMedico() : Observable<Medico> {
    return this.citaMedico.asObservable();
  }


  // Diagnostico
  setDiagnostico(diagnostico : Diagnostico) {
    this.diagnosticoActual.next(diagnostico);
  }

  getDiagnostico() : Observable<Diagnostico> {
    return this.diagnosticoActual.asObservable();
  }
  

  // Para recargar el usuario actual, tras añadir una cita
  // Realizamos una llamada a GET /api/pacientes/id
  updatePaciente() {
    var pacienteId = '';
    this.getPaciente().subscribe(p => {
      pacienteId = p.usuario;
    })

    this.http.get('/api/pacientes/' + pacienteId).subscribe(p => {
      this.setPaciente(p as Paciente);
    })
  }
}
