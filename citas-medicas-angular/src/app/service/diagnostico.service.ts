import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Diagnostico } from '../model/diagnostico';

@Injectable({
  providedIn: 'root'
})
export class DiagnosticoService {

  private diagnosticoUrl : string = '/api/diagnosticos';

  constructor(
    private http : HttpClient
  ) { }


  public getDiagnostico(id : string) : Observable<Diagnostico>  {
    
    // Devolvemos get /api/diagnostico/id 
    return this.http.get<Diagnostico>(this.diagnosticoUrl + '/' + id);
  }

  public updateDiagnostico(d : Diagnostico) : Observable<Diagnostico> {
    // PUT /api/diagnosticos d
    return this.http.put<Diagnostico>(this.diagnosticoUrl, d);
  }
}
