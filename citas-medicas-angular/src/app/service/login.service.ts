import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario } from '../model/usuario';
import { Paciente } from '../model/paciente';
import { Medico } from '../model/medico';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  
  private loginUrl : string = '/api/usuarios';

  constructor(
    private http : HttpClient
    ) { }

   public findAll(): Observable<Usuario[]> {
     // Devolvemos GET /api/usuarios
    return this.http.get<Usuario[]>(this.loginUrl);
  }

  public findUsuario(user : Usuario) : Observable<Paciente | Medico>  { 
    // Devolvemos POST /api/usuarios (usuario)
    return this.http.post<Paciente | Medico>(this.loginUrl, user);
  }

}
