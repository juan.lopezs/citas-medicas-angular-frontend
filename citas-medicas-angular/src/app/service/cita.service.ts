import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cita } from '../model/cita';

@Injectable({
  providedIn: 'root'
})
export class CitaService {
  
  private citaUrl : string = '/api/citas';

  constructor(
    private http : HttpClient
  ) { }


  public createCita(cita : Cita) : Observable<Cita>  {
    // Devolvemos POST /api/citas (usuario)
    return this.http.post<Cita>(this.citaUrl, cita);
  }
}
