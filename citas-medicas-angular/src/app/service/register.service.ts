import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Medico } from '../model/medico';
import { Medico2 } from '../model/medico2';
import { Paciente } from '../model/paciente';
import { Paciente2 } from '../model/paciente2';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(
    private http : HttpClient
  ) { }

  private urlPaciente : string = '/api/pacientes';
  private urlMedico : string = '/api/medicos';
  

  public registrarPaciente(paciente : Paciente2) : Observable< Paciente > {
    // Devolvemos POST /api/pacientes (usuario)
    return this.http.post< Paciente >(this.urlPaciente, paciente);
  }

  public registrarMedico(medico : Medico2) : Observable< Medico > {
    // Devolvemos POST /api/medicos (usuario)
    return this.http.post< Medico >(this.urlMedico, medico);
  }

}
