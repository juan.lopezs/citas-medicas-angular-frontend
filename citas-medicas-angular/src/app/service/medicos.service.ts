import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Medico2 } from '../model/medico2';

@Injectable({
  providedIn: 'root'
})
export class MedicosService {

  private urlMedico : string = '/api/medicos';
  
  constructor(
    private http : HttpClient
  ) { }

  public getMedicos() : Observable< Medico2[] > {
    // Devolvemos GET /api/medicos 
    return this.http.get<Medico2[]>(this.urlMedico);

  }
}
