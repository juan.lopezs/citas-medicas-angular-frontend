import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from "./components/login/login.component";
import { RegisterComponent } from "./components/register/register.component";
import { AppComponent } from "./app.component";
import { MainComponent } from './components/main/main.component';
import { RegMedicoComponent } from './components/register/reg-medico/reg-medico.component';
import { RegPacienteComponent } from './components/register/reg-paciente/reg-paciente.component';
import { PacienteComponent } from './components/paciente/paciente.component';
import { MedicoComponent } from './components/medico/medico.component';
import { CommonModule } from '@angular/common';
import { PacienteCitaComponent } from './components/paciente/paciente-cita/paciente-cita.component';
import { PacienteDiagComponent } from './components/paciente/paciente-diag/paciente-diag.component';
import { MedicoDiagComponent } from './components/medico/medico-diag/medico-diag.component';

const routes: Routes = [
  { path : '', component : MainComponent, pathMatch : "full" },
  { path : 'login', component : LoginComponent},
  { path : 'register', component : RegisterComponent, 
    children : [
      { path : 'paciente', component : RegPacienteComponent},
      { path : 'medico', component : RegMedicoComponent}
    ]},
  { path : 'paciente', component : PacienteComponent},
  { path : 'paciente-cita', component : PacienteCitaComponent},
  { path : 'paciente-diag', component : PacienteDiagComponent},

  { path : 'medico', component : MedicoComponent},
  { path : 'medico-diag', component : MedicoDiagComponent}
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
