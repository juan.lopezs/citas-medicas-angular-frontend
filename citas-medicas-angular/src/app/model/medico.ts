import { Cita } from "./cita";
import { Paciente } from "./paciente";

export class Medico{
    
    usuario : string;
    clave : string;
    nombre : string;
    apellidos : string;

    numColegiado : string;

    citas : Cita[];
    pacientes : Paciente[];

    constructor(usuario : string, clave : string, nombre : string,
        apellidos : string, numColegiado : string, citas : Cita[], pacientes : Paciente[]){
            this.usuario = usuario;
            this.clave = clave;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.numColegiado = numColegiado;
            this.citas = citas;
            this.pacientes = pacientes;
    }

}