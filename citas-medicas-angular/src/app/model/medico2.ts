export class Medico2{
    
    usuario : string;
    clave : string;
    nombre : string;
    apellidos : string;

    numColegiado : string;

    constructor(usuario : string, clave : string, nombre : string,
        apellidos : string, numColegiado : string){
            this.usuario = usuario;
            this.clave = clave;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.numColegiado = numColegiado;
    }
}