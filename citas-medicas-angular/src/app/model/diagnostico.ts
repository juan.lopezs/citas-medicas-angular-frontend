export class Diagnostico{

    id : number;
    valoracionEspecialista : string;
    enfermedad : string;
    cita : number;
}