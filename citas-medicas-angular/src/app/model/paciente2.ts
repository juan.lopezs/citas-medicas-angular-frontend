export class Paciente2{

    usuario : string;
    nombre : string;
    apellidos : string;
    
    nss : string;
    numTarjeta : string;
    telefono : string;
    direccion : string;

    constructor(usuario : string, nombre : string,
        apellidos : string, nss : string, numTarjeta : string,
        telefono : string, direccion : string){
            this.usuario = usuario;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.nss = nss;
            this.numTarjeta = numTarjeta;
            this.telefono = telefono;
            this.direccion = direccion;
    }
}