import { Cita } from "./cita";
import { Medico } from "./medico";
import { Medico2 } from "./medico2";

export class Paciente{

    usuario : string;
    clave : string;
    nombre : string;
    apellidos : string;
    
    nss : string;
    numTarjeta : string;
    telefono : string;
    direccion : string;

    medicos : Medico[];
    citas : Cita[];
    
    constructor(usuario : string, clave : string, nombre : string,
        apellidos : string, nss : string, numTarjeta : string,
        telefono : string, direccion : string, medicos : Medico[], citas : Cita[]){
            this.usuario = usuario;
            this.clave = clave;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.nss = nss;
            this.numTarjeta = numTarjeta;
            this.telefono = telefono;
            this.direccion = direccion;
            this.medicos = medicos;
            this.citas = citas;
    }
}