export class Cita{
    id : number;
    fechaHora : Date;
    motivoCita : string;
    pacienteId : string;
    medicoId : string;
    diagnosticoId : string;
}