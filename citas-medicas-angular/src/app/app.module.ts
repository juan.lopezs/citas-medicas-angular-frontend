import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainComponent } from './components/main/main.component';
import { RegMedicoComponent } from './components/register/reg-medico/reg-medico.component';
import { RegPacienteComponent } from './components/register/reg-paciente/reg-paciente.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { PacienteComponent } from './components/paciente/paciente.component';
import { PacienteCitaComponent } from './components/paciente/paciente-cita/paciente-cita.component';
import { MedicoComponent } from './components/medico/medico.component';
import { PacienteDiagComponent } from './components/paciente/paciente-diag/paciente-diag.component';
import { MedicoDiagComponent } from './components/medico/medico-diag/medico-diag.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    RegisterComponent,
    RegMedicoComponent,
    RegPacienteComponent,
    PacienteComponent,
    PacienteCitaComponent,
    MedicoComponent,
    PacienteDiagComponent,
    MedicoDiagComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
